"use strict"



// Змінні та типи даних JavaScript




// Змінні 

//  var mayVar = "same info" ; створюємо змінну далі даємо назву і через дорівнює присвоюємо якесь значення зміні.

//  const myVar1 = "same info" ; створюємо змінну далі даємо назву і через дорівнює присвоюємо якесь значення зміні але змінну const ми в подальшому незможемо перезаписувати навідміно від змінної let

//  let myVar2 = "same info" ; створюємо змінну далі даємо назву і через дорівнює присвоюємо якесь значення змінs . Змінна let має таку властивість як перезаписуватись тобто ми можемо її записати а потім перезаписати нове значення в цю зміну.





//  Типи данних


// const myNumber = 123; - тип данних число ;

// const myStr = "text string"; - тип данних рядок ;

// const myBool = true ; - логічний тип даний містить тільки два значення true або false ;

// const myNull = null; - тип даних null кий немістить значення ;

// const myUndef = undefined; - тип даних при якому Java Script незміх знайти дані які шукав 



// Складні типи данних 

// const obj = {name ; Rostyk,
            // age : 23,}; - тип данних обєкт ;

// const  arr = [1,2,3,4,5,6,7,8,9]; - тип данних обєкт хоть пишеться в масиві ;

// const func = function name() {}; - тип данних функція ;


//  Для тогго щоб взнати тип даних ми використовуємо оператор typeof


// const myNumber = 123;
// const myStr = "text string";
// const myBool = true;
// const myNull = null;
// const myUndef = undefined;
// const obj = {name ; Rostyk,
    // age : 23,}; - тип данних обєкт ;
// const  arr = [1,2,3,4,5,6,7,8,9];
// const func = function name() {};


// console.log(typeof myNumber);
// console.log(typeof myStr);
// console.log(typeof myBool);
// console.log(typeof myNull); - тип данних null дає значення object це є помилковим значенням
// console.log(typeof myUndef);
// console.log(typeof abj);
// console.log(typeof arr); - тип даних обєкт
// console.log(typeof func);





//                                               Взаємодія із користувачем 



// alert('Rostyk') - задопомогою alert ми можемо показати якесь повідомлення користувачові за допомогою модального вікна

// confirm("Тобі 18 ?") - за допомогою цього методу ми можемо спитати щось у користувача  користувач має можливість відповісти Ok або Cancle і ми отримуємо при натискані на Ok true при натискані на Cancle false

// prompt("Як тебе звати ?") - ми можемо щось запитати в користувача і при цьому методі користувач має змогу дати відповідь  запишавши її у модальному вікні  якщо користувач нічого ненапише і нажме Оk вернеться пустий рядок  якщо нажме Cancle вернеться null





//                                                    Перетворення типів 

// const num = 3;
// const one = 1;
// const zero = 0;
// const str = "4";
// const str2 = "asdf"
// const emptStr = "";
// const arr = [1,2,3]
// const emptArr = [];
// const obj = {age : 18};
// const emptObj = {};
// const nl = null;
// const undef = undefined;
// const notNumber = NaN;

//                                                           Рядковий тип перетворення 

// console.log(String(num), typeof String(num));
// console.log(String(one), typeof String(one));
// console.log(String(zero), typeof String(zero));
// console.log(String(str), typeof String(str));
// console.log(String(str2), typeof String(str2));
// console.log(String(emptStr), typeof String(emptStr));
// console.log(String(arr), typeof String(arr)); - масив при перетворенні виймає значення із квадратних дужок та стає рядком
// console.log(String(emptArr), typeof String(emptArr)); - порожній масив перетворюється в порожній рядок
// console.log(String(obj), typeof String(obj)); - перетворюється на Object
// console.log(String(emptObj), typeof String(emptObj)); - перетворюється на Object
// console.log(String(nl), typeof String(nl)); - null буде null але рядком
// console.log(String(undef), typeof String(undef)); - undefined але буде рядком
// console.log(String(notNumber), typeof String(notNumber)); - NaN але буде рядком


//                                                Методи перетворення в рядок   

// 1 - String(), 

// 2 - .toString(), 

// 3 -  concat "+"  - конкатонація за допомогою оператора +  вона виконується тільки тоді коли біля  + з ліва чи з права є значення яке має тип String



//                                                      Чисельний тип перетворення


// console.log(Number(num), typeof Number(num));
// console.log(Number(one), typeof Number(one));
// console.log(Number(zero), typeof Number(zero));
// console.log(Number(str), typeof Number(str)); - при перетворенні число в рядку перетворилось на число
// console.log(Number(str2), typeof Number(str2)); - при перетворені літер у число отримаємо NaN тому що літери не можна перетворити на цифри
// console.log(Number(emptStr), typeof Number(emptStr)); - при перетворені пустий рядок перетворюється на 0 число 
// console.log(Number(arr), typeof Number(arr)); - при перетворені масива ми отримаємо NaN
// console.log(Number(emptArr), typeof Number(emptArr)); - при перетворені порожнього масиву ми отримаємо 0 число
// console.log(Number(obj), typeof Number(obj)); - при перетворені обєкту ми отримоємо  NaN томущо обєкти неперетворюються у число 
// console.log(Number(emptObj), typeof Number(emptObj)); - при перетворені пустого обєкту ми отримаємо NaN томущо обєкт не перетворюється на число
// console.log(Number(nl), typeof Number(nl)); - при перетворені null  ми отримаємо 0
// console.log(Number(undef), typeof Number(undef)); - при перетворені undefined ми отримаємо  NaN
// console.log(Number(notNumber), typeof Number(notNumber)); - при перетворення NaN ми отримаємо NaN

//                                    Методи перетвореня  

// 1 - parseFloat("3.14") - цей метод перетворить число в рядок у вигляді дробу оскільки записано через кому
// 2 - parseInt("3.14") - цей метод перетворить число в ціле число  



//                                            Логічний тип перетворення


// console.log(Boolean(num), typeof Boolean(num)); - true
// console.log(Boolean(one), typeof Boolean(one)); - true
// console.log(Boolean(zero), typeof Boolean(zero)); - false
// console.log(Boolean(str), typeof Boolean(str)); - true
// console.log(Boolean(str2), typeof Boolean(str2)); - true
// console.log(Boolean(emptStr), typeof Boolean(emptStr)); - false
// console.log(Boolean(arr), typeof Boolean(arr)); - true
// console.log(Boolean(emptArr), typeof Boolean(emptArr)); -true
// console.log(Boolean(obj), typeof Boolean(obj)); -true
// console.log(Boolean(emptObj), typeof Boolean(emptObj)); - true
// console.log(Boolean(nl), typeof Boolean(nl)); - false
// console.log(Boolean(undef), typeof Boolean(undef)); -false
// console.log(Boolean(notNumber), typeof Boolean(notNumber)); - false


//                                                  Логічні методити перетворення 

// 1- Boolean() 

// 2 - Логічні оператори  !-заперечення     || - або   && - і (також)

// 3 - Логічні вирази if(){}




//                                                  Оператори порівняння


// == , === ,  != , !===   - є чотири оператори порівняння



//                                                            НЕСУВОРЕ ПОРІВНЯННЯ     ==
// console.log(1 == 1); - буде true
// console.log(1 == '1'); - буде true тому що порівняння не є суворим 
// console.log(1 == true); - буде true тому що при кастингові типів true перетвориться в 1  і тоді прирівняється 
// console.log(1 == "1a"); - буде false тому що при порівнянні друге значення буде мати NaN томущо його неможна перетворити в число


//                                                             СУВОРЕ ПОРІВНЯННЯ     ===
// console.log(1 === 1); - буде true 
// console.log(1 === '1'); - буде false тому що при суворому порівнянні  ці два значення будуть мати різні типи даних перший числовий другий рядковий
// console.log(1 === true); - буде false
// console.log(1 === '1a'); - буде false тому що перше значення буде числове а друге NaN


//                                                 Оператор не до рівнює !=

// console.log(1 != 1); - це буде false оскільки перше значення дорівнює другому значині 
// console.log(1 != '1'); - це буде false оскільки  перше значення дорівнює другому оскільки це не дорівнює не є суворим 
// console.log(1 != true); - це буде false
// console.log(1 != '1a'); - це буде true тому що дійсно перше значення недорівнює другому


//                                                Оператор не дорівнює суворо    !==

// console.log(1 !== 1); - буде false оскільки  перше значення строго дорівнює другому значеню
// console.log(1 !== '1'); - буде true оскільки перше значення строго недорівнює другому значенню  перше значення Number  друге String
// console.log(1 !== true); - буде true
// console.log(1 !== '1a'); - буде true томущо перше значенню дійсно недорівнює другому значенню


















//                                                      ЗАДАЧІ та РОЗВЯЗАННЯ

// Завдання 1

// Створити змінну, зі значенням 5.
// Створити другу змінну зі значенням 10.
// Створити третю змінну, до якої присвоїти суму
// двох перших змінних
// Вивести у консоль цю суму

// Відповідь :

// const number1 = 5;
// const number2 = 10;

// const suma = number1 + number2;
// console.log(suma);



// Завдання 2

// Отримати від користувача його ім'я.
// Записати отримане значення змінну
// з назвою userName.
// Вивести значення змінної на екран за допомогою функції alert

// Відповідь :

// const userName = prompt('First Name')
// alert("userName");




// Завдання 3 

// Вивести в консоль результат порівняння числа 24 та рядок
// Зі значенням '24'. Вивести результат суворого та не суворого порівнянь

//  Відповідь :

// const numberNum = 24;
// const numberStr = '24';
// console.log(numberNum === numberStr); - суворе порівняння ;
// console.log(numberNum == numberStr); - не сурове порівняння ;



// Завдання 4

// Отримати від користувача його ім'я.
// Записати отримане значення в змінну
// з назвою userName. Отримати від користувача його прізвище.
// Записати до змінної. Вивести у консоль фразу
// `Ваше ім'я ${значення змінної імені}, прізвище - ${значення змінної прізвища}

// Відповідь :

// const userName = prompt("Ваше ім'я");
// const userSurname = prompt("Ваше прізвище");
// console.log(`Ваше ім'я ${userName}, прізвище ${userSurname}`);



// Завдання 5


// За допомогою модального вікна prompt отримати від користувача два числа.
// Вивести в консоль суму, різницю, результат множення, результат поділу та залишок від поділу їх один на одного.

// Відповідь :

// const numberOne = prompt("Напишіть перше число");
// const numberTwo = prompt("Напишіть друге число");
// console.log(Number(numberOne) + Number(numberTwo));
// console.log(numberOne - numberTwo);
// console.log(numberOne * numberTwo);
// console.log(numberOne / numberTwo);
// console.log(numberOne % numberTwo);
