"use strict"

// 1
const paragraphs = document.querySelectorAll('p');
paragraphs.forEach(element => element.classList.add('active'));


// 2
const optionsList = document.getElementById('optionsList');
console.log(optionsList);

const parents = optionsList.parentElement;
console.log(parents);

const ulChildNodes = document.querySelector('#optionsList').childNodes;
if(ulChildNodes !== true){
    ulChildNodes.forEach(node => console.log(`Type: ${node.nodeType}, Name: ${node.nodeName}`))
}
console.log(ulChildNodes);


// 3
const content = document.querySelector('#testParagraph');
content.innerText= 'This is a paragraph';
console.log(content);


// 4 
const elementNewClass = document.querySelector(".main-header").childNodes;
elementNewClass.forEach(element => {
if(element.nodeName !== '#text') element.classList.add("nav-item")
})
console.log(elementNewClass);


// 5 
const elementDelete = document.querySelectorAll('.section-title');
elementDelete.forEach(element => element.classList.remove('section-title'));
console.log(elementDelete);
