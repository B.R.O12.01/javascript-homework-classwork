'use strict'


let firstName = prompt('Як вас звати ?');
let lastName = prompt('Яке ваше прізвище ?');

let createNewUser = (firstName, lastName) => {

  const newUser = {
    firstName,
    lastName,
    getLogin () {
      return this.firstName.slice(0, 1).toLocaleLowerCase() + this.lastName.toLocaleLowerCase();
    },
  }

  return newUser;
};


const user1 = createNewUser(firstName,lastName);

console.log(user1.getLogin());
