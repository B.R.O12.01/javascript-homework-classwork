"use strict"


let firstName = prompt('Як вас звати ?');
let lastName = prompt('Яке ваше прізвище ?');
let birthday = prompt('Запиши свою дату народження у вигляді m.d.y');

let createNewUser = (firstName, lastName, birthday) => {

  const newUser = {
    firstName,
    lastName,
    birthday,

    getLogin () {

      return this.firstName.slice(0, 1).toLocaleLowerCase() + this.lastName.toLocaleLowerCase();

    },

    getYear () {

      let parts = this.birthday.split(".");
      let day = parts[0];
      let month = parts[1];
      let yearNew = parts[2];
      let date = new Date(yearNew, month - 1, day);
      let dataNow = new Date();
 
      
      let year = dataNow.getFullYear() - date.getFullYear();

      date.setFullYear(dataNow.getFullYear());

      if(dataNow < date){

        year--

      }

      this.birthday = year;  

      return this.birthday;

  },

  getPassword () {

    return this.firstName.slice(0,1).toUpperCase() + this.lastName.toLocaleLowerCase() + this.birthday;

  },

}

    return newUser;

}

const user1 = createNewUser(firstName,lastName, birthday);
console.log(user1.getLogin());
console.log(user1.getYear());
console.log(user1.getPassword());


