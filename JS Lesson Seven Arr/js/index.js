// "use strict"

// // 1
// // Створити масив об'єктів students (у кількості 7 прим).
// // У кожного студента має бути ім'я, прізвище та напрямок навчання - Full-stask
// // чи Front-end. Кожен студент повинен мати метод, sayHi, який повертає рядок
// // `Привіт, я ${ім'я}, студент Dan, напрям ${напрямок}`.
// // Перебрати кожен об'єкт і викликати у кожного об'єкта метод sayHi;



// const arr = [
//     {
//         name : 'Jhon',
//         lastName :'Bogach',
//         student :'Front-End',
//         sayHi (){
//             return console.log(`Привіт я ${this.name} ${this.lastName} студент Dan, напрямок ${this.student} `);
//         }
//     },

//     {
//         name : 'Oleg',
//         lastName :'Farenyuk',
//         student : 'Full - Stak',
//         sayHi (){
//             return console.log(`Привіт я ${this.name} ${this.lastName} студент Dan, напрямок ${this.student} `);
//         }
//     },

//     {
//         name : 'Olya',
//         lastName :'Drozdova',
//         student : 'Front - End',
//         sayHi (){
//             return console.log(`Привіт я ${this.name} ${this.lastName} студент Dan, напрямок ${this.student} `);
//         }
//     },

//     {
//         name : 'Rostyk',
//         lastName :'Borovyk',
//         student : 'Front - End',
//         sayHi (){
//             return console.log(`Привіт я ${this.name} ${this.lastName} студент Dan, напрямок ${this.student} `);
//         }
//     },

//     {
//         name : 'Stanislav',
//         lastName :'Zelenskiy',
//         student : 'Full - Stak',
//         sayHi (){
//             return console.log(`Привіт я ${this.name} ${this.lastName} студент Dan, напрямок ${this.student} `);
//         }
//     },

//     {
//         name : 'Yura',
//         lastName :'Mykhaylov',
//         student : 'Full - Stak',
//         sayHi (){
//             return console.log(`Привіт я ${this.name} ${this.lastName} студент Dan, напрямок ${this.student} `);
//         }
//     },

//     {
//         name : 'Jhon',
//         lastName :'Jonson',
//         student : 'Front - End',
//         sayHi (){
//             return console.log(`Привіт я ${this.name} ${this.lastName} студент Dan, напрямок ${this.student} `);
//         }
//     },
// ]   

// const newArr = arr.forEach((value) => {
//     return value.sayHi()
// })




// Є масив брендів автомобілів ["bMw", "Audi", "teSLa", "toYOTA"].
// Вам потрібно отримати новий масив, об'єктів типу
// {
//   type: 'car'
//   brand: ${елемент масиву}
// }

// Вивести масив у консоль

// const arr = ['Bmw','Audi','Tesla','Toyota'];
// const objBMW = {
//     type : 'car',
//     brand : `${arr[0]}`
// }
// const objAudi = {
//     type : 'car',
//     brand : `${arr[1]}`
// }
// const objTesla = {
//     type : 'car',
//     brand : `${arr[2]}`
// }
// const objToyota = {
//     type : 'car',
//     brand : `${arr[3]}`
// }
// const newArr = [];
// newArr.push(objBMW);
// newArr.push(objAudi);
// newArr.push(objTesla);
// newArr.push(objToyota);
// console.log(newArr);




// Створити масив чисел від 1 до 100.
// Відфільтрувати його таким чином, щоб до нового масиву не потрапили числа менше 10 і більше 50.
// Вивести у консоль новий масив.

// const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50];

// const  newArr = arr.filter(element => element >= 10 && element <= 40);
// console.log(newArr);




// Опис завдання: Напишіть функцію, яка порівнює два масиви і повертає true,
// якщо вони ідентичні.

const arr = (arrOne,arrTwo) => arrOne.join(',').localeCompare() === arrTwo.join(',').localeCompare() ? console.log(true) : console.log(false);
arr([1,2,3,4,5],[1,2,3,4,5]);
arr([5,4,3,2,1],[1,2,3,4,5]);
arr([1,2,3,4,5],["1","2","3","4","5"]);






