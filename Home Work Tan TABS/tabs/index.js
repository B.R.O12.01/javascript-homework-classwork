"use strict"

const tabs = document.querySelectorAll('.tabs-title')
const text = document.querySelectorAll('.content-text');


for(let buttonTabs of tabs){
  buttonTabs.addEventListener('click', (event) =>{
    tabs.forEach(iteam =>{
      iteam.classList.remove('active')
    })
    event.target.classList.add('active')
    for(let activeText of text){
      if(activeText.dataset.text === event.target.dataset.tabs){
        activeText.classList.add('active');
      }else{
        activeText.classList.remove('active');
      }
    }
  })
};
