"use strict"

const btn = document.querySelectorAll('button');

window.addEventListener('keydown', event => {
    btn.forEach(element => {
        element.classList.remove('active');
        if(event.key === element.dataset.tab){
            element.classList.add('active');
        }
    })
});
